from collections import deque
import numpy as np
import matplotlib.pyplot as plt

#default values
iterations = 3
size = 3
view_fractal = True

options = 8 #0,1,2,3
# 0 nothing
# 1 True
# 2 x flip
# 3 y flip
# 4 xy flip
# 5 rotation +90
# 6 rotation -90
# 7 constant

decay = 0 # decay in percent

def fractal_create(original: np.ndarray, iterations: int):
    '''
    for each iteration replaces each coloured pixel in the pattern with the pattern
    '''
    ori_shape = original.shape
    new_shape = np.power(ori_shape, iterations)

    if np.all(original==0):
        return np.full(new_shape, 0)
    elif (np.all(original >= 1) and np.all(original <= 6)):
        return np.full(new_shape, 1)
    elif np.all(original == 7):
        return np.full(new_shape, 2)

    pattern = np.full(new_shape, 0, dtype=int)

    existing_fractal = original
    ex_frac_shape = ori_shape

    queue = deque()
    non_z_list = []

    # put all non-zero values in a List
    for a in range(ori_shape[0]):
        for b in range(ori_shape[1]):
            if original[a,b] > 0:
                non_z_list.append([a,b])

    # put all non-zero/seven values in a Queue
    for a in range(ex_frac_shape[0]):
        for b in range(ex_frac_shape[1]):

            if existing_fractal[a,b] <= 0:
                continue

            #create a block if 7
            if existing_fractal[a,b] == 7:
                pattern[a*ori_shape[0]**(iterations-1) : (a+1)*ori_shape[0]**(iterations-1), b*ori_shape[1]**(iterations-1) : (b+1)*ori_shape[1]**(iterations-1)] = 2
            
            #put into queue for iterating
            else:
                queue.append([  (a)*ori_shape[0]**(iterations-1),   #x_start
                                (b)*ori_shape[1]**(iterations-1),   #y_start
                                iterations-2,                           #iteration
                                existing_fractal[a,b],                  #value  
                                [0,0,0]])                               #rotation           
    
    #repeat until done
    while len(queue) != 0:

        temp = queue.popleft()
        if temp[2] <= -1:
            pattern[temp[0]][temp[1]] = 1
            continue

        rotation = temp[4][0]
        X_FLIP = temp[4][1]
        Y_FLIP = temp[4][2]
        if temp[3] == 2:
            X_FLIP = not X_FLIP
        elif temp[3] == 3:
            Y_FLIP = not Y_FLIP
        elif temp[3] == 4:
            X_FLIP = not X_FLIP
            Y_FLIP = not Y_FLIP
        elif temp[3] == 5:
            rotation = (rotation+1)%4
        elif temp[3] == 6:
            rotation = (rotation-1)%4


        for el in non_z_list:

            if np.random.rand() <= decay/100:
                    continue

            value = original[el[0],el[1]]

            x = temp[0]+el[0]*ori_shape[0]**temp[2]
            y = temp[1]+el[1]*ori_shape[1]**temp[2]

            if value == 7:
                pattern[x : temp[0]+(el[0]+1)*ori_shape[0]**(temp[2]), y : temp[1]+(el[1]+1)*ori_shape[1]**(temp[2])] = 2
                continue

            if X_FLIP:
                x = temp[0]+(ori_shape[0]-1-el[0])*ori_shape[0]**temp[2]
            if Y_FLIP:
                y = temp[1]+(ori_shape[1]-1-el[1])*ori_shape[1]**temp[2]
            elif rotation == 1:
                x = temp[0]+(ori_shape[1]-1-el[1])*ori_shape[1]**temp[2]
                y = temp[1]+el[0]*ori_shape[0]**temp[2]
            elif rotation == 2:
                x = temp[0]+(ori_shape[0]-1-el[0])*ori_shape[0]**temp[2]
                y = temp[1]+(ori_shape[1]-1-el[1])*ori_shape[1]**temp[2]
            elif rotation == 3:
                x = temp[0]+el[1]*ori_shape[1]**temp[2]
                y = temp[1]+(ori_shape[0]-1-el[0])*ori_shape[0]**temp[2]

            queue.append([  x,
                            y,
                            temp[2]-1,
                            value,
                            [rotation, X_FLIP, Y_FLIP]])

    return pattern

pattern = np.full((size,size), 0, dtype=int)

draw_fractal = fractal_create(pattern, iterations).T

fig = plt.figure(figsize=(11, 11))  # instantiate a figure to draw
ax = plt.axes()  # create an axes object

def onclick(event):
    '''
    click handler:
    leftclick: negate value in Array
    rightclick: negate Array
    '''
    global pattern, draw_fractal
    if event.xdata == None or event.ydata == None:
        return
    x_lim = ax.get_xlim()
    y_lim = ax.get_ylim()

    if event.button == 1:
        x = int(event.xdata/x_lim[1]*size)
        y = int(event.ydata/y_lim[1]*size)
        pattern[x][y] = (pattern[x][y]+1)%options
    elif event.button == 3:
        x = int(event.xdata/x_lim[1]*size)
        y = int(event.ydata/y_lim[1]*size)
        pattern[x][y] = (pattern[x][y]-1)%options
    elif event.button == 2:
        draw_fractal = (draw_fractal-1)%3
        update(False)
        return
    update()

def update(create: bool = True):
    global draw_fractal, pattern, iterations, view_fractal

    if create:
        draw_fractal = fractal_create(pattern, iterations).T
    if view_fractal:
        plt.pcolormesh(draw_fractal, shading = 'flat', cmap="Greys",vmin=0,vmax=2)
    else:
        plt.pcolormesh(pattern.T, shading = 'flat', cmap="Greys",vmin=0,vmax=7)
    fig.canvas.draw()

def onkey(event):
    if not event.inaxes:
        return
    global iterations, decay, pattern, draw_fractal, view_fractal, size

    if event.key == "J":
        if iterations < 10:
            iterations += 1
    elif event.key == "j":
        if iterations > 2:
            iterations -= 1
            fig.gca().clear()
    elif event.key == "A":
        pattern = (pattern+1)%options
    elif event.key == "a":
        pattern = (pattern-1)%options
    elif event.key == "D":
        if decay < 95:
            decay += 5
        else:
            return
    elif event.key == "d":
        if decay >= 5:
            decay -= 5
        else:
            return
    elif event.key == "c":
        view_fractal = not view_fractal
        fig.gca().clear()
        update(False)
        return
    elif event.key == "B":
        pattern = np.append(pattern, np.zeros((1,size)), axis=0)
        size += 1
        pattern = np.append(pattern, np.zeros((size,1)), axis=1)
    elif event.key == "b":
        if size > 2:
            size -= 1
            pattern = pattern[:size,:size]
            fig.gca().clear()
        else:
            return
    elif event.key == "x":
        pattern = fractal_create(pattern, 2)
        iterations = 2
        size = pattern.shape[0]
    else:
        return
    update()

update()

frac = plt.pcolormesh(pattern, cmap="Greys")

ax.set(xlim=(0, size), ylim=(0, size))
ax.get_xaxis().set_visible(False)
ax.get_yaxis().set_visible(False)

cid = fig.canvas.mpl_connect('button_press_event', onclick)
fig.canvas.mpl_connect('key_press_event', onkey)

plt.show()